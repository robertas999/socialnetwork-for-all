<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feeds;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\File;
class Feed extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feed_posts = DB::table('feeds')->orderBy('created_at', 'desc')->get();
        $users = DB::table('users')->get();
        return view('welcome', ['feed_posts' => $feed_posts, 'users' => $users]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // managing image upload

    public function upload(Request $request)
    {
        if(Input::hasFile('file')){

			$file = Input::file('file');
            // $file->move('uploads', $file->getClientOriginalName());
            $code_db = new Feeds;
            $path = $request->file('file')->store('public');
            $code_db->newfeed = $path;
            $code_db->userid = auth()->id();
            $code_db->username = auth()->user()->name;
            $code_db->save();
			return back();
		}

	        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $post=new DB;
        // $post->newfeed=$request->newfeed;
        // $post->userid = auth()->id(); // add this line
        // $post->save();

        $code_db = new Feeds;
        $code_db->newfeed = $request->input('newfeed');
        $code_db->userid = auth()->id();
        $code_db->username = auth()->user()->name;
        $code_db->save();
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // show profile on profile page
        $userinfo = DB::table('users')->where('id', $id)->first();
        return view('profile.profile', ['userinfo' => $userinfo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('feeds')->where('id', $id)->delete();
        return back();
    }
}
