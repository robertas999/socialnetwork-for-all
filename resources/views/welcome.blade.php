@extends('layouts.app')

@section('content')
<br>
    @auth
    {{-- <h1 class="display-5">
        Your feed:
    </h1> --}}
    <br>
    <div>
        <p class="h5">Tell us something</p>
            <form action="/socialnetwork/public/feed" method="post">
                @csrf
            <input type="text" name="newfeed" class="css-input" required>
            <input class="btn btn-success" type="submit" value="Post">
            </form>
            <br>
            <form action="{{ URL::to('upload') }}" method="post" enctype="multipart/form-data">
                <label>Or upload an image:</label>
                <input type="file" name="file" id="file">
                <input type="submit" value="Upload" name="submit" class="btn btn-success">
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
            </form>
    </div>
    <br>
    <br>
    
    {{-- strpos($$post->newfeed, '.jpg') !== false --}}

        
    
        @foreach ($feed_posts as $post)
            @if ($post->userid === auth()->id())
                
            <div class="speech-bubble"><p><li style="list-style-type: none;" class="h6">{{$post->newfeed}} </li>
            <img src="{{$post->newfeed}}" onerror="this.style.display='none'" style="width:200px;">
            </p></div>
            
                    <form method="post" action="feed/{{ $post->id }}">
                        {{ method_field('DELETE') }}
                        <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <br>
                        <button type="submit" class="btn btn-primary btn-sm " id="buttonnew" style="float:right; margin-top:-13px;">Delete</button>
                        </div>  
                        </form>
            
                {{-- <li style="list-style-type: none;"> <span class="text-muted">posted by:</span>  <span class="text-success">{{$post->username}}</span></li> --}}
                <br>
            @else
            <div class="speech-bubble2"><p><li style="list-style-type: none;" class="h6">{{$post->newfeed}} </li></div>
                <li style="list-style-type: none;"> <span class="text-muted" style="float:right;"> says</span>  <a href=feed/{{$post->userid}} style="text-decoration: none;"><span class="text-success" style="float:right;">{{$post->username}}</span></li></a>
                <br></p>
            @endif

        @endforeach

    @endauth
    @guest
    <br>
    <figure class="figure">
    <img src="{{asset('images/samoa-island.jpg')}}" class="figure-img img-fluid rounded" alt="...">
            <figcaption class="figure-caption">Please <a href="{{ route('login') }}">Login</a> to see..</figcaption>
          </figure>
    @endguest

    

@endsection

@section('links')
<br>
<br>
@auth
    

    <div class="card">
        <img src="{{ asset('images/sidebar.jpg') }}" class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">betterPlace Social network</h5>
            <p class="card-text">Social network made using better place blueprint</p>
            {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
            </div>
  </div>
@endsection
@endauth
@section('users')
<br>
    @auth
        
    <br>
    <h1 class="display-5">
        Users:
    </h1>
    <br>
    <ol>
        @foreach ($users as $user)
    <li><a href="feed/{{$user->id}}">{{$user->name}}</a></li>
        @endforeach
    </ol>

    @endauth
@endsection