@extends('layouts.app')

@section('content')
<br>
User name: <p class="h3">{{$userinfo->name}}</p>
<br>
Email: <p class="h3">{{$userinfo->email}}</p>
<br>
Created On: <p class="h3">{{$userinfo->created_at}}</p>
<br>
<br>
<a href="{{ URL::previous() }}">Back</a>
    
@endsection