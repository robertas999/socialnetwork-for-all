Social network prototype build using laravel framework.

**Current functionality:**

* New user registration
* Protected content from unregistered users
* Crud functionality with sql database
* Image upload
* Display registered users.

<img src="https://i.imgur.com/2dRVhVs.png"/>


